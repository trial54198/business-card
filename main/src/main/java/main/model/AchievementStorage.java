package main.model;

import org.springframework.data.repository.CrudRepository;

public interface AchievementStorage extends CrudRepository<Achievement, Integer> {
}
