package main.model;

import org.springframework.data.repository.CrudRepository;

public interface SummaryStorage extends CrudRepository<Summary, Integer> {
}
