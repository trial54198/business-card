package main.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Achievement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String type;

    private String description;

    @Column(name = "achieve_date")
    private LocalDateTime achieveDate;

    @ManyToOne(cascade = CascadeType.ALL)
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getAchieveDate() {
        return achieveDate;
    }

    public void setAchieveDate(LocalDateTime achieveDate) {
        this.achieveDate = achieveDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
