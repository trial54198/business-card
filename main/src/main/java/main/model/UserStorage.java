package main.model;

import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;

public interface UserStorage extends CrudRepository<User, Integer> {
}
