package main.controllers;

import main.model.User;
import main.model.UserStorage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;

@RestController
public class UserController {

    private final UserStorage userStorage;

    UserController(UserStorage userStorage){
        this.userStorage = userStorage;
    }

    @PostMapping("/api/user")
    public ResponseEntity<?> add(@RequestBody User user) {
        user.setCreateDate(LocalDateTime.now());
        User newToDo = userStorage.save(user);
        return new ResponseEntity<>(newToDo, HttpStatus.OK);
    }

    @PostMapping("/api/user/{id}")
    public ResponseEntity<?> add(@PathVariable int id) {
        throw new MethodNotAllowed();
    }

    @PutMapping("/api/user/{id}")
    public ResponseEntity<?> edit(@RequestBody User user, @PathVariable int id) {
        User editableUser = getUserById(id);
        if(editableUser == null){
            throw new UserNotFound();
        }
        editableUser.setEmail(user.getEmail());
        editableUser.setPhone(user.getPhone());
        userStorage.save(editableUser);
        return new ResponseEntity<>(editableUser.getId(), HttpStatus.OK);
    }

    @DeleteMapping("/api/user/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        if(!userStorage.findById(id).isPresent()){
            throw new UserNotFound();
        }
        userStorage.deleteById(id);
        return new ResponseEntity<>(userStorage.count(), HttpStatus.OK);
    }

    @GetMapping("/api/user/{id}")
    public ResponseEntity<?> get(@PathVariable int id) {
        User requestUser = getUserById(id);
        if(requestUser == null){
            throw new UserNotFound();
        }
        return new ResponseEntity<>(requestUser, HttpStatus.OK);
    }

    @GetMapping("/api/user")
    public ResponseEntity<?> getAll() {
        Iterable all = userStorage.findAll();
        return new ResponseEntity<>(all, HttpStatus.OK);
    }


    public User getUserById(int id){
        Optional optional = userStorage.findById(id);
        return optional.isPresent() ? (User)optional.get() : null;
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "User not found")
    public class UserNotFound extends RuntimeException{}

    @ResponseStatus (code = HttpStatus.METHOD_NOT_ALLOWED, reason = "Method not allowd")
    public class MethodNotAllowed extends RuntimeException{}
}
