package main.controllers;

import main.model.User;
import main.model.UserStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

public class MainController {

    @Autowired
    private UserStorage userStorage;

    @Value("${project.pagetitle.index}")
    private String indexTitle;

    @RequestMapping("/")
    public String index(Model model) {

        Iterable<User> iterable = userStorage.findAll();
        ArrayList<User> userList = new ArrayList<>();
        for (User toDo : iterable) {
            userList.add(toDo);
        }
        model.addAttribute("userList", userList);
        model.addAttribute("indexTitle", indexTitle);
        return "index";
    }

}
